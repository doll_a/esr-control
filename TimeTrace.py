import numpy as np
import h5py
datapath = "C:\\Users\\adria\\Documents\\Lernmaterial\\Physik\\Solid_State_Physics\\Masterarbeit\\Data\\rawdata\\"
import scipy.fft as fft
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os
from scipy import interpolate
from scipy.signal import savgol_filter
from lmfit import Model, Parameters, Parameter, Minimizer
import pandas as pd

class TimeTrace:

    def __init__(self):

        self.t = np.array([])
        self.s = np.array([], dtype=complex)

    def loadnpz(self, filename):
        loadz = np.load(filename)
        self.t= loadz['s_x']
        self.s = loadz['s_y']

    def get_DC_phase(self):
        return np.angle(np.mean(self.s))

    def fouriertransform(self, microwave_phase, freq):

        srot = self.s*np.exp(-1j*microwave_phase)
        signal = np.exp(1j*self.t*freq*2*np.pi)

        ref = np.sum((np.real(srot)-np.mean(np.real(srot)))*signal)/len(signal)
        imf = np.sum((np.imag(srot)-np.mean(np.imag(srot)))*signal)/len(signal)

        return ref, imf
    def absfouriertransform(self, freq):
        signal = np.exp(1j*self.t*freq*2*np.pi)
        return np.sum(np.abs(self.s)*signal)/len(signal)

    def fouriertransform_fast(self, microwave_phases, freq):
        srot = self.s*np.exp(-1j*microwave_phases.reshape((len(microwave_phases),1)))
        signal = np.exp(1j*self.t*freq*2*np.pi)
        ref = np.sum((np.real(srot)-np.mean(np.real(srot)))*signal, axis=1)
        imf = np.sum((np.imag(srot)-np.mean(np.imag(srot)))*signal, axis=1)
        return ref, imf

    def FFT(self, normalized=False):
        N = len(self.s)
        dt = self.t[1]-self.t[0]
        f = 1/(N*dt)*np.arange(0, N)
        if normalized:
            return f, fft.fft(self.s/np.abs(np.mean(self.s)))
        else:
            return f, fft.fft(self.s)/N
    def noise(self, fmin, fmax):
        f, A = self.FFT()
        B = np.logical_and(f > fmin, f < fmax)
        return 10*np.log10(np.mean(np.abs(A[B])**2)*len(f))

    def plot(self, ax, start=0, end=-1):

        ax.plot(self.t[start:end], (np.real(self.s)-np.mean(np.real(self.s)))[start:end], label="Real part", color="k")
        ax.plot(self.t[start:end], (np.imag(self.s)-np.mean(np.imag(self.s)))[start:end], label="Imaginary part", color="r")

    def DC(self):
        return np.mean(self.s)

class Spectrum:

    def __init__(self):

        self.spectrum_number = None
        self.mod_freq = None
        self.res_freq = None
        self.time_traces = []
        self.current = []
        self.measfield = None
        self.field = None
        self.dispersion = np.array([])
        self.absorption = np.array([])
        self.smooth = True
        self.microphase = None
        self.modphase = None
        self.calibrationI = None
        self.calibrationB = None

    #Deletable?
    def load_spectrum_information(self, spectrum_number, spectra):
        self.spectrum_number = spectrum_number
        self.mod_freq = spectra.loc[spectrum_number]["Modulation frequency"]

    def load_hdf5(self, filename):
        """
        Parameters
        ----------
        filename : str
        Filename of the spectrum rawdata saved with hdf5.

        Returns
        -------
        None
        """
        with h5py.File(filename, "r") as f:
            self.mod_freq = np.array(f["Modulation frequency"])
            self.res_freq = np.array(f["Resonance frequency"])

            self.current = np.array(f["Current"])
            if "Field" in f.keys():
                self.measfield = np.array(f["Field"])
            if self.calibrationI is not None:
                self.field = self.calibrationI(self.current)

            for i in range(len(f["Traces"].keys())):
                TT = TimeTrace()
                TT.t = np.array(f["Traces"]["Trace{:d}".format(i)]["t"])
                TT.s = np.array(f["Traces"]["Trace{:d}".format(i)]["s"])
                self.time_traces.append(TT)
        return None

    def cut_traces(self, start, end):
        for TT in self.time_traces:
            TT.t = TT.t[start:end]
            TT.s = TT.s[start:end]

    def cut_spectrum(self, start, end, mode="points"):
            if mode=="points":
                if self.dispersion is not None:
                    return self.dispersion[start:end], self.absorption[start:end]
                else:
                    self.time_traces = self.time_traces[start:end]
                    self.current = self.current[start:end]
                if self.field is not None:
                    self.field = self.field[start:end]
                if self.measfield is not None:
                    self.measfield = self.measfield[start:end]
            elif mode=="current":
                A = np.logical_and(self.current > start, self.current < end)

                self.time_traces = [self.time_traces[i] for i in range(len(self.time_traces)) if A[i]]
                self.current = self.current[A]
                if self.field is not None:
                    self.field = self.field[A]
                if self.measfield is not None:
                    self.measfield = self.measfield[A]

                if len(self.dispersion) > 0:
                    return self.dispersion[A], self.absorption[A]

            elif mode=="field":
                A = np.logical_and(self.field > start, self.field < end)

                self.time_traces = [self.time_traces[i] for i in range(len(self.time_traces)) if A[i]]
                self.current = self.current[A]
                self.field = self.field[A]
                if self.measfield is not None:
                    self.measfield = self.measfield[A]
                if len(self.dispersion) > 0:
                    return self.dispersion[A], self.absorption[A]
    #Deletable?
    def load_time_traces(self, traces):
        for idx, t in traces.loc[traces["Spectrum index"] == self.spectrum_number].iterrows():
            TTtmp = TimeTrace()
            TTtmp.loadnpz(t["Filename"])
            self.time_traces.append(TTtmp)
            self.current.append(t["Current"])

        self.current = np.array(self.current)

    def import_calibration(self, name, caylar_path=""):
        """
        Imports a magnetic field vs. coil current calibration.

        Parameters
        ----------
        name : str
        Either filename of the calibration file or one of the four
        Caylar calibrations: "Caylar_large_holder", "Caylar_large_center",
        "Caylar_small_holder", "Caylar_small_center"

        Returns
        -------
        None
        """

        if name=="Caylar_large_holder":
            data = pd.read_excel(caylar_path+"Caylar_calibration.xlsx", sheet_name="field_vs_current_gap80D10mm_V1_", usecols=range(0, 7))
            B= np.array(data["Column4"][3:], dtype=float)
            I = np.array(data["Column2"][3:], dtype=float)
        elif name=="Caylar_large_center":
            data = pd.read_excel(caylar_path+"Caylar_calibration.xlsx", sheet_name="field_vs_current_gap80D10mm_V1_", usecols=range(0, 7))
            B = np.array(data["Column6"][3:], dtype=float)
            I = np.array(data["Column2"][3:], dtype=float)
        elif name=="Caylar_small_holder":
            data = pd.read_excel(caylar_path+"Caylar_calibration.xlsx", usecols=range(0, 7))
            I = np.array(data["Column2"][3:], dtype=float)
            B = np.array(data["Column4"][3:], dtype=float)
        elif name=="Caylar_small_center":
            data = pd.read_excel(caylar_path+"Caylar_calibration.xlsx", usecols=range(0, 7))
            B = np.array(data["Column6"][3:], dtype=float)
            I = np.array(data["Column2"][3:], dtype=float)
        else:
            A = np.loadtxt(name)
            I = A[:,0]
            B = A[:,1]
        self.calibrationB = interpolate.interp1d(B, I)
        self.calibrationI = interpolate.interp1d(I, B)

        return None

    def get_DC_phase(self):
        """
        Calculates the average DC phase of all the time traces.

        Parameters
        ----------

        Returns
        -------
        float
            average microwave phase
        """
        microphase = 0
        for TT in self.time_traces:
            microphase += TT.get_DC_phase()
        self.microphase = microphase/len(self.time_traces)

        return self.microphase
    def DC(self):
        s = 0
        for TT in self.time_traces:
            s+=TT.DC()
        return s/len(self.time_traces)

    def DC_spectrum(self):
        self.dispersion = np.zeros(len(self.time_traces))
        self.absorption = np.zeros(len(self.time_traces))
        for i, TT in enumerate(self.time_traces):
            s = TT.DC()
            self.dispersion[i] = np.real(s)
            self.absorption[i] = np.imag(s)
        self.dispersion -= self.dispersion[0]
        self.absorption -= self.absorption[0]

    def abs_spectrum(self, microphase=None):
        """
        Calculates absolute value of dispersion and absorption.

        Parameters
        ----------
        microphase: float, default=None
        Microwavephase used for correction, if None DC phase + pi/2 is used.

        Returns
        -------
        float
            absolute value of dispersion
        float
            absolute value of absorption
        float
            phase of signal dispersion signal
        float
            phase of absorption signal
        """
        absdisp = np.zeros(len(self.time_traces))
        absabsorp = np.zeros(len(self.time_traces))

        angledisp = np.zeros(len(self.time_traces))
        angleabsorp = np.zeros(len(self.time_traces))

        if microphase is None:
            self.microphase = self.get_DC_phase()+np.pi/2
        else:
            self.microphase = microphase

        for i, TT in enumerate(self.time_traces):
            ref, imf = TT.fouriertransform(microphase, self.mod_freq)

            angledisp[i] = np.angle(ref)
            angleabsorp[i] = np.angle(imf)

            absdisp[i] = np.abs(ref)
            absabsorp[i] = np.abs(imf)


        return absdisp, absabsorp, angledisp, angleabsorp

    def complex_spectrum(self, microphase=None):
        disp = np.zeros(len(self.time_traces), dtype=complex)
        absorp = np.zeros(len(self.time_traces), dtype=complex)


        if microphase is None:
            self.microphase = self.get_DC_phase()+np.pi/2
        else:
            self.microphase = microphase

        for i, TT in enumerate(self.time_traces):
            disp[i], absorp[i] = TT.fouriertransform(microphase, self.mod_freq)

        self.dispersion = disp
        self.absorption = absorp

        return self.dispersion, self.absorption

    def spectrum_background_subtracted(self, background=None, microphase=None, modphase=None):

        self.complex_spectrum(microphase=microphase)
        bd = 0
        ba = 0
        if background is None:
            L = len(self.dispersion)
            idx = int(L*0.5)
            bd = np.mean(self.dispersion[:idx])+np.mean(self.dispersion[-idx:])
            ba = np.mean(self.absorption[:idx])+np.mean(self.absorption[-idx:])
        else:
            for (start, end) in background:
                A = np.logical_and(self.field>start, self.field < end)
                if not np.all(np.logical_not(A)):
                    bd += np.mean(self.dispersion[A])
                    ba += np.mean(self.absorption[A])

        if modphase is None:
            self.modphase = self.calc_modphase(angledisp=angledisp, angleabsorp=angleabsorp)
        else:
            self.modphase = modphase

        self.dispersion -= bd
        self.absorption -= ba

        absdisp = np.abs(self.dispersion)
        absabsorp = np.abs(self.absorption)
        angledisp = np.angle(self.dispersion)
        angleabsorp = np.angle(self.absorption)
        self.dispersion = absdisp*np.cos(angledisp-self.modphase)
        self.absorption = absabsorp*np.cos(angleabsorp-self.modphase)

        return angledisp, angleabsorp

    def calc_modphase(self, angledisp=None, angleabsorp=None, offset=0):
        """
        Calculates an estimate of the modulation phase.

        Parameters
        ----------
        angledisp: float np.array
        The argument of the real part time trace fouriertransform.

        angleabsorp: float np.array
        The argument of the imaginary part time trace fouriertransform.

        offset: float, default=0
        Offset the zero point.

        Returns
        -------
        float
            estimation of the modulation phase
        """
        if angledisp is None or angleabsorp is None:
            _, _, angledisp, angleabsorp = self.abs_spectrum()
        self.modphase = 0.5*(np.mean(np.mod(angledisp+offset, np.pi))+np.mean(np.mod(angleabsorp+offset, np.pi)))-offset

        return self.modphase

    def spectrum(self, microphase=None, offset=0, modphase=None, method="sign"):
        """
        Calculates the dispersion and absorption of the spectrum.

        Parameters
        ----------
        microphase: float default=None
        Microphase passed to abs_spectrum()

        offset: float default=0
        Offset passed to modphase()

        modphase: float default=None
        Fixes the value of the modulation phase, if None it is
        estimated by the modphase function.

        Returns
        -------
        np.array float
            Dispersion
        np.array float
            Absorption
        """
        self.dispersion, self.absorption, angledisp, angleabsorp = self.abs_spectrum(microphase=microphase)

        if modphase is None:
            self.modphase = self.calc_modphase(angledisp=angledisp, angleabsorp=angleabsorp)
        else:
            self.modphase = modphase

        if method=="sign":
            A, B = self.match_modulation_phase(angledisp, angleabsorp)

            self.dispersion[A]*=-1
            self.absorption[B]*=-1
        elif method=="projection":
            self.dispersion = self.dispersion*np.cos(angledisp-self.modphase)
            self.absorption = self.absorption*np.cos(angleabsorp-self.modphase)

        return self.dispersion, self.absorption

    def match_modulation_phase(self, angledisp, angleabsorp):
        """
        Matches the actual to either phi_mod or -phi_mod

        Parameters
        ----------
        angledisp: float np.array
        The argument of the real part time trace fouriertransform.

        angleabsorp: float np.array
        The argument of the imaginary part time trace fouriertransform.

        Returns
        -------
        np.array bool
            True if phase is closer to phi_mod, False if closer to -phi_mod for
            dispersion

        np.array bool
            True if phase is closer to phi_mod, False if closer to -phi_mod for
            absorption
        """

        A = np.abs(np.exp(1j*angledisp) -np.exp(1j*self.modphase))<np.sqrt(2)
        B = np.abs(np.exp(1j*angleabsorp) -np.exp(1j*self.modphase))<np.sqrt(2)

        if self.smooth:
            A = self.smooth_phase(A, 5)
            B = self.smooth_phase(B, 5)

        return A, B

    def smooth_phase(self, x, N):
        """
        Modified moving average filter.

        Parameters
        ----------
        x: np.array bool
        Data to be averaged.

        N: int
        Window length

        Returns
        -------
        np.array bool
            Smoothed data
        """
        cumsum = np.cumsum(np.append(np.insert(x, 0, [0]*int(N/2)),[0]*int(N/2)))
        cumsum = np.insert(cumsum, 0, 0)
        return (cumsum[N:] - cumsum[:-N]) / float(N) > 0.5

    def optimize_phase(self, npoints=50):
        """
        Calculates the average microwave phase of all the time traces.

        Parameters
        ----------
        npoints: int, default=50
        Number of points used for optimizing

        Returns
        -------
        float
            returns optimized microwave phase
        """

        phases = np.linspace(0, np.pi, npoints)
        summe = np.zeros_like(phases)
        for ii, ph in enumerate(phases):
            self.spectrum(microphase=ph)
            summe[ii] += np.sum(self.absorption)

        self.microphase = phases[np.argmin(np.abs(summe))]
        self.spectrum(microphase=self.microphase)
        return self.microphase

    def fast_optimize_phase(self):
        """
        WIP: Computationaly optimized version of optimize phase.

        """
        phases = np.linspace(0, np.pi, 50)
        absabsorp = np.zeros((len(self.time_traces),len(phases)))
        angleabsorp = np.zeros((len(self.time_traces),len(phases)))


        for i, TT in enumerate(self.time_traces):
            ref, imf = TT.fouriertransform_fast(phases, self.mod_freq)
            angleabsorp[i] = np.angle(imf)
            absabsorp[i] = np.abs(imf)

        modphase = np.mean(np.mod(angleabsorp, np.pi), axis=1)
        B = np.abs(np.exp(1j*angleabsorp)-np.exp(1j*modphase.reshape((len(modphase),1))))<np.sqrt(2)
        absabsorp[B] *= -1
        summe = np.sum(absabsorp, axis=0)
        self.microphase = phases[np.argmin(np.abs(summe))]
        self.spectrum(microphase=self.microphase)

        return self.microphase

    def dispersion_line(self, H, H0, Hpp, D):
        return D*3*(3-((H-H0)*2/Hpp)**2)/(3+((H-H0)*2/Hpp)**2)**2

    def absorption_line(self, H, H0, Hpp, A):
        return 16*A*(H-H0)*2/Hpp/(3+((H-H0)*2/Hpp)**2)**2

    def complex_line(self, parvals):
        return np.exp(1j*parvals["muphase"])*(self.dispersion_line(self.field, parvals["H0"], parvals["Hpp"], parvals["D"])+parvals["Doffset"]
        +1j*self.absorption_line(self.field, parvals["H0"], parvals["Hpp"], parvals["A"])+1j*parvals["Aoffset"])

    def complex_line_fit(self, minHpp=0.1, maxHpp=3000, H0guess=None, phaseguess=0, background=True):

        if H0guess is None:
            H0guess = self.field[np.argmax(self.dispersion**2+self.absorption**2)]
        Hppguess = np.abs(self.linewidth(mode="field"))
        Dguess = np.max(np.abs(self.dispersion))
        Aguess = np.max(np.abs(self.absorption))

        params = Parameters()
        params['H0'] = Parameter(name='H0', value=H0guess, max=np.max(self.field), min=np.min(self.field))
        if minHpp is not None and maxHpp is not None:
            params["Hpp"] = Parameter(name='Hpp', value=Hppguess, min=minHpp, max=maxHpp)
        else:
            params["Hpp"] = Parameter(name='Hpp', value=Hppguess, min=0)

        params["A"] = Parameter(name="A", value=Aguess)
        params["D"] = Parameter(name='D', value=Dguess)
        params["muphase"] = Parameter(name="muphase", value=phaseguess, min=-np.pi, max=np.pi)
        if background:
            params["Doffset"] = Parameter(name="Doffset", value=0)
            params["Aoffset"] = Parameter(name="Aoffset", value=0)
        else:
            params["Doffset"] = Parameter(name="Doffset", value=0, vary=False)
            params["Aoffset"] = Parameter(name="Aoffset", value=0, vary=False)
        def residual(pars, data=None, eps=None):
        # unpack parameters: extract .value attribute for each parameter
            parvals = pars.valuesdict()


            signaldiff = self.complex_line(parvals)-self.dispersion-1j*self.absorption
            Atmp = np.array([np.real(signaldiff), np.imag(signaldiff)])
            return Atmp.flatten()

        #f = lambda Q, f0, delay: np.sum(np.abs(phase_complex(Spar.frequency, Q, f0, delay) -np.exp(1j*Spar.phase))**2)
        minob = Minimizer(residual, params)
        res = minob.minimize()

        return res

    def absolute_line_square(self, H, H0, Hpp, A, D):
        return self.dispersion_line(H, H0, Hpp, D)**2+ self.absorption_line(H, H0, Hpp, A)**2

    def absolute_line_fit(self):

        mo= Model(self.absolute_line_square)

        H0guess = self.field[np.argmax(self.dispersion**2+self.absorption**2)]
        Hppguess = self.linewidth(mode="field")
        Dguess = np.max(np.abs(self.dispersion))
        Aguess = np.max(np.abs(self.absorption))
        res = mo.fit(self.dispersion**2+self.absorption**2, H=self.field, H0=H0guess, Hpp=Hppguess, A=Aguess, D=Dguess)

        return res

    def complex_spectrum_model(self, parvals):
        signal = np.exp(1j*parvals["muphase"])*(self.dispersion_line(self.field, parvals["H0"], parvals["Hpp"], parvals["D"])
        +1j*self.absorption_line(self.field, parvals["H0"], parvals["Hpp"], parvals["A"]))
        disp = np.real(signal)+ parvals["Doffset"]*np.exp(1j*parvals["Doffsetphase"])
        absorp = np.imag(signal)+ parvals["Aoffset"]*np.exp(1j*parvals["Aoffsetphase"])
        return disp, absorp

    def complex_spectrum_fit(self, minHpp=0.1, maxHpp=3000, H0guess=None, phaseguess=np.pi, background=True):

        if H0guess is None:
            H0guess = self.field[np.argmax(self.dispersion**2+self.absorption**2)]
        Hppguess = np.abs(self.linewidth(mode="field"))
        Dguess = np.max(np.abs(self.dispersion))
        Aguess = np.max(np.abs(self.absorption))

        params = Parameters()
        params['H0'] = Parameter(name='H0', value=H0guess, max=np.max(self.field), min=np.min(self.field))
        params["Hpp"] = Parameter(name='Hpp', value=Hppguess, min=minHpp, max=maxHpp)
        params["A"] = Parameter(name="A", value=Aguess)
        params["D"] = Parameter(name='D', value=Dguess)
        params["muphase"] = Parameter(name="muphase", value=phaseguess, min=0, max=2*np.pi)
        params["modphase"] = Parameter(name="modphase", value=2.411, vary=False)

        if background:
            Doffsetguess = np.mean(np.angle(self.dispersion[-5:]))
            Aoffsetguess = np.mean(np.angle(self.absorption[-5:]))
            params["Doffset"] = Parameter(name="Doffset", value=0)
            params["Aoffset"] = Parameter(name="Aoffset", value=0)
            params["Doffsetphase"] = Parameter(name="Doffsetphase", value=Doffsetguess)
            params["Aoffsetphase"] = Parameter(name="Aoffsetphase", value=Aoffsetguess)
        else:
            params["Doffset"] = Parameter(name="Doffset", value=0, vary=False)
            params["Aoffset"] = Parameter(name="Aoffset", value=0, vary=False)
            params["Doffsetphase"] = Parameter(name="Doffsetphase", value=0, vary=False)
            params["Aoffsetphase"] = Parameter(name="Aoffsetphase", value=0, vary=False)

        def residual(pars, data=None, eps=None):
            parvals = pars.valuesdict()

            disp, absorp = self.complex_spectrum_model(parvals)
            signaldiffdisp = disp-self.dispersion
            signaldiffabsorp = absorp - self.absorption
            Atmp = np.array([np.real(signaldiffdisp), np.imag(signaldiffdisp), np.real(signaldiffabsorp), np.imag(signaldiffabsorp)])
            return Atmp.flatten()

        #f = lambda Q, f0, delay: np.sum(np.abs(phase_complex(Spar.frequency, Q, f0, delay) -np.exp(1j*Spar.phase))**2)
        minob = Minimizer(residual, params)
        res = minob.minimize()

        return res

    def noise_std(self, start, end):

        di, ab = self.cut_spectrum(start, end, mode="current")
        return np.sqrt(np.std(di)**2+ np.std(ab)**2)*np.sqrt(len(self.time_traces[0].t))

    def background(self, start, end):
        di, ab = self.cut_spectrum(start, end, mode="current")
        return np.sqrt(np.mean(di)**2 +np.mean(ab)**2)*0.5

    def subtract_background(self):
        stabs = np.mean(self.absorption[:5])
        endabs = np.mean(self.absorption[-5:])
        self.absorption -= np.arange(len(self.absorption))*(endabs-stabs)/len(self.absorption)+stabs

        stdi= np.mean(self.dispersion[:5])
        enddi = np.mean(self.dispersion[-5:])
        self.dispersion -= np.arange(len(self.absorption))*(enddi-stdi)/len(self.absorption)+stdi

    def filter_spectrum(self, wl=100, order=5):
        self.dispersion = savgol_filter(self.dispersion, wl, order)
        self.absorption = savgol_filter(self.absorption, wl, order)

    def absorption_curve(self, flip=True):
        spacing = np.diff(self.current)
        spacing = np.append(spacing, spacing[-1])
        abscurve =  np.cumsum(self.absorption*spacing)
        if flip and np.abs(np.min(abscurve)) > np.max(abscurve):
            abscurve *=-1

        return abscurve

    def linewidth(self, mode="current"):
        idx1 = np.argmax(self.absorption)
        idx2 = np.argmin(self.absorption)
        if mode=="current":
            return self.current[idx2]-self.current[idx1]
        elif mode=="field":
            return self.field[idx2]-self.field[idx1]

    def resonance_field(self, mode="current"):
        idx1 = np.argmax(self.dispersion)
        if mode=="current":
            return self.current[idx1]
        elif mode=="field":
            return self.field[idx1]

    def ppA(self):
        return np.max(self.absorption)-np.min(self.absorption)

    def plot_spectrum(self, ax, start=0, spectrum_parts=["Dispersion", "Absorption"], xaxis="current"):
        fontsize=12

        if xaxis=="current":
            x =self.current[start:]
            xlim =[self.current[start:][0], self.current[start:][-1]]
        elif xaxis=="field":
            x=self.field[start:]
            xlim = [self.field[start:][0], self.field[start:][-1]]
        elif xaxis=="measured field":
            x=self.measfield[start:]
            xlim = [self.measfield[start:][0], self.measfield[start:][-1]]

        if "Dispersion" in spectrum_parts:
            ax.plot(x, self.dispersion[start:], color="r", label="Dispersion", linewidth=3)

        if "Absorption" in spectrum_parts:
            ax.plot(x, self.absorption[start:], color="k", label="Absorption", linewidth=3)

        ax.legend(fontsize=fontsize)
        ax.grid()
        ax.set_xlabel("Magnet coil current [A]", fontsize=fontsize)
        ax.set_ylabel("Amplitude (arb. units)", fontsize=fontsize)
        ax.set_title("Derivative spectrum", fontsize=fontsize)
        ax.tick_params(axis='both', which='major', labelsize=fontsize)
        ax.set_xlim(xlim)

    def plot_absorption(self, ax, start=0):
        abscurve = self.absorption_curve()
        fontsize=24
        ax.plot(self.current[start:], abscurve[start:], color="k", label="Absorption", linewidth=3)
        ax.legend(fontsize=fontsize)
        ax.grid()
        ax.set_xlabel("Magnet coil current [A]", fontsize=fontsize)
        ax.set_ylabel("Amplitude (arb. units)", fontsize=fontsize)
        ax.tick_params(axis='both', which='major', labelsize=18)
        ax.set_xlim([self.current[start:][0], self.current[start:][-1]])

    def plot_phases(self, ax, start=0, microphase=None, plot_type="single", modpi=False, color=None, **keywords):
        if microphase is None and self.microphase is not None:
            microphase = self.microphase

        if color is None:
            color = ["r", "k"]

        absdisp, absabsorp, angledisp, angleabsorp = self.abs_spectrum(microphase=microphase)
        A, B = self.match_modulation_phase(angledisp, angleabsorp)
        fontsize=12

        if not modpi:
            di = angledisp[start:]
            ab = angleabsorp[start:]
        else:
            di = np.mod(angledisp[start:], np.pi)
            ab = np.mod(angleabsorp[start:], np.pi)

        if plot_type == "simpledisp":
            if "label" in keywords.keys():
                ax.plot(self.current[start:], di, label=keywords["label"], linewidth=3)
            else:
                ax.plot(self.current[start:], di, label=keywords["label"], linewidth=3)
        elif plot_type == "simpleabsorp":
            if "label" in keywords.keys():
                ax.plot(self.current[start:], ab, label=keywords["label"], linewidth=3)
            else:
                ax.plot(self.current[start:], ab, label=keywords["label"], linewidth=3)

        elif plot_type=="single":
            ax.plot(self.current[start:], di, color="r", label="Dispersion", linewidth=3)
            ax.plot(self.current[start:], ab, color="k", label="Absorption", linewidth=3)

            if not modpi:
                ax.plot(self.current[start:], A*np.pi-np.pi+self.modphase, color="g", label="Dispersion sign", linewidth=3)
                ax.plot(self.current[start:], B*np.pi-np.pi+self.modphase, color="gray", label="Absorption sign", linewidth=3)

        ax.legend(fontsize=fontsize)
        ax.grid()
        ax.set_xlabel("Magnet coil current [A]", fontsize=fontsize)
        ax.set_ylabel("Phases", fontsize=fontsize)
        ax.set_title("Phases", fontsize=30)
        ax.tick_params(axis='both', which='major', labelsize=18)
        ax.set_xlim([self.current[start:][0], self.current[start:][-1]])


class SParameterSpectrum:

    def __init__(self):
        self.frequency = None
        self.s = None
        self.loss = None
        self.phase = None

    #def load_s2p(self, filename):

    def load_npz(self, filename):

        loadz = np.load(filename)
        self.frequency = loadz['s_x']
        self.s= loadz['s_y']
        self.loss = 20*np.log10(np.abs(self.s))
        self.calc_phase(0)
    def cut_trace(self, start, end, mode="points"):
        if mode=="points":
            self.frequency = self.frequency[start:end]
            self.s = self.s[start:end]
            self.loss = self.loss[start:end]
            self.phase = self.phase[start:end]

        elif mode=="frequency":
            A = np.logical_and(self.frequency > start, self.frequency < end)
            self.frequency = self.frequency[A]
            self.s = self.s[A]
            self.loss = self.loss[A]
            self.phase = self.phase[A]
    def calc_phase(self, delay, offset=0):
        self.phase = np.mod(np.angle(self.s)-2*np.pi*delay*self.frequency-offset, 2*np.pi)-np.pi
        self.s = self.s*np.exp(-1j*(2*np.pi*delay*self.frequency+offset))
    def plot_trace(self, ax):
        fontsize=24
        ax.plot(self.frequency/1e9, self.loss)
        ax.grid()
        ax.set_xlabel("Frequency [GHz]", fontsize=fontsize)
        ax.set_ylabel("Loss [dB]", fontsize=fontsize)
        ax.set_xlim(self.frequency[0]/1e9, self.frequency[-1]/1e9)
        #ax.set_title("Transmission (S12) from input to output",fontsize=30)
        ax.tick_params(axis='both', which='major', labelsize=18)

    def plot_phase(self, ax, delay=0):
        fontsize=24
        ax.plot(self.frequency/1e9, self.phase*180/np.pi)
        ax.grid()
        ax.set_xlabel("Frequency [GHz]", fontsize=fontsize)
        ax.set_ylabel("Phase (deg)", fontsize=fontsize)
        ax.set_xlim(self.frequency[0]/1e9, self.frequency[-1]/1e9)
        #ax.set_title("Transmission (S12) from input to output",fontsize=30)
        ax.tick_params(axis='both', which='major', labelsize=18)

def find_npz_file(datafolder, name):
    return [f for f in os.listdir(datafolder) if (name in f and '.npz' in f)]

class FieldSweepSeries:

    def __init__(self, filenames, calibration="..\..\Code\caylar_class\large_cryostathighBDPA.txt", caylar_path=""):

        self.spectra = []

        for fn in filenames:
            sp = Spectrum()
            sp.import_calibration(calibration, caylar_path=caylar_path)
            sp.load_hdf5(fn)
            self.spectra.append(sp)
        self.fit_results = []
    def analyse_spectra(self, microphase=0, modphase=None):
        for sp in self.spectra:
            if microphase == "DC90deg":
                sp.spectrum(microphase=sp.get_DC_phase(), modphase=modphase)
            elif microphase == "DC":
                sp.spectrum(microphase=sp.get_DC_phase()+np.pi/2, modphase=modphase)
            else:
                sp.spectrum(microphase=microphase, modphase=modphase)

    def plot_overview(self, ax, label=False):
        for sp in self.spectra:
            sp.DC_spectrum()
            ax[3].plot(sp.field, sp.absorption, linewidth=3, label=str(sp.res_freq))

            sp.spectrum(microphase=0)
            ax[0].plot(sp.field, sp.dispersion, linewidth=3, label=str(sp.res_freq))
            ax[1].plot(sp.field, sp.absorption, linewidth=3, label=str(sp.res_freq))
            ax[2].plot(sp.field, np.sqrt(sp.absorption**2+sp.dispersion**2), linewidth=3, label=str(sp.res_freq))

        ax[0].set_ylabel("Dispersion amplitude (arb. units)")
        ax[1].set_ylabel("Absorption amplitude (arb. units)")
        ax[2].set_ylabel("Absolute amplitude (arb. units)")
        ax[3].set_ylabel("DC amplitude (arb. units)")

        for a in ax:
            a.set_xlabel("Magnetic field [G]")
            if label:
                a.legend()

    def get_resonance_field(self, method="max", minHpp=0.1):
        self.analyse_spectra()
        if method == "max":
            field = []
            for sp in self.spectra:
                idx = np.argmax(np.sqrt(sp.absorption**2+sp.dispersion**2))
                field.append(sp.field[idx])
            return np.array(field)
        elif method == "fit":
            if len(self.fit_results) == 0:
                self.fit_complex(minHpp=minHpp)
            return np.array([res.params["H0"].value for res in self.fit_results])


    def automatic_fit(self, idx=None, chisqthreshold=0.2, chisqit=2, normalize=True, changephase=True, minHpp=200, maxHpp=3000):
        self.analyse_spectra(microphase="DC")
        fitresult1 = self.fit_complex(idx=idx, minHpp=300, maxHpp=maxHpp)

        if changephase:
            self.analyse_spectra(microphase="DC90deg")
            fitresult2 = self.fit_complex(idx=idx, minHpp=300, maxHpp=maxHpp)

            fitresulttmp = []
            for res1, res2 in zip(fitresult1, fitresult2):
                if res1.chisqr > res2.chisqr:
                    fitresulttmp.append(res2)
                else:
                    fitresulttmp.append(res1)
        else:
            fitresulttmp = fitresult1

        chisq = np.array([res.chisqr for res in fitresulttmp])
        if normalize:
            if idx is not None:
                maxsig = [np.max(np.sqrt(sp.dispersion**2+sp.absorption**2)) for ii, sp in enumerate(self.spectra) if idx[ii]]
            else:
                maxsig = [np.max(np.sqrt(sp.dispersion**2+sp.absorption**2)) for ii, sp in enumerate(self.spectra)]
            chisq = chisq/maxsig

        A = np.array([True]*len(fitresult1))
        for ii in range(chisqit):
            A = (chisq-np.mean(chisq[A]))/np.mean(chisq[A]) < chisqthreshold

        self.fit_results = [res for ii, res in enumerate(fitresulttmp) if A[ii]]

        return self.fit_results, fitresulttmp, chisq, A

    def fit_complex(self, minHpp=0.1, maxHpp=3000, idx=None):
        self.fit_results = []
        for i, sp in enumerate(self.spectra):
            if idx is None:
                self.fit_results.append(sp.complex_line_fit(minHpp=minHpp, maxHpp=maxHpp))

            else:
                if idx[i]:
                    self.fit_results.append(sp.complex_line_fit(minHpp=minHpp, maxHpp=maxHpp))
        return self.fit_results

    def get_microwave_phases(self, method="DC"):
        mphase= []

        if method=="fit":
            if len(self.fit_results) == len(self.spectra):
                for res in self.fit_results:
                    mphase.append(res.params["muphase"].value)
        elif method=="optimize":
            for sp in self.spectra:
                sp.optimize_phase()
                mphase.append(sp.microphase)

        elif method=="DC":
            for sp in self.spectra:
                mphase.append(np.angle(sp.DC()))
        return np.array(mphase)

    def get_DC_part(self):
        DCS11 = []

        for sp in self.spectra:
            DCS11.append(sp.DC())
        return np.array(DCS11)

    def get_microwave_frequencies(self):
        f =[]

        for sp in self.spectra:
            f.append(sp.res_freq)

        return np.array(f)

    def frequency_cut(self, H):

        #???
        absorp = []
        disp = []
        for sp in self.spectra:
            difftmp = np.diff(sp.field)
            if len(difftmp) > 0:
                dH = np.max(np.abs(difftmp))

                diff = np.abs(sp.field-H)
                idx = np.argmin(diff)
                if diff[idx] > dH: #check if there exist values close to H
                    absorp.append(0)
                    disp.append(0)
                else:
                    absorp.append(sp.absorption[idx])
                    disp.append(sp.dispersion[idx])
            else:
                absorp.append(0)
                disp.append(0)

        return np.array(disp), np.array(absorp)

    def frequency_cut_spectrum(self, H, lwmin=1e-5, lwmax=100, f0guess=None, background=True):

        f = self.get_microwave_frequencies()
        disp, absorp = self.frequency_cut(H)
        sp = Spectrum()
        S11norm=(disp +1j*absorp)/self.get_DC_part()

        sp.field = f
        sp.dispersion = np.real(S11norm)
        sp.absorption = np.imag(S11norm)


        return f, disp, absorp, sp.complex_line_fit(minHpp=lwmin, maxHpp=lwmax, H0guess=f0guess, background=background), sp

    def get_line_widths(self, method="complex", minHpp=0.1, maxHpp=3000, idx=None):
        linewidths = []

        if len(self.fit_results) > 0:
            if method!="fitpp":
                for res in self.fit_results:
                    linewidths.append(res.params["Hpp"].value*np.sqrt(3))
            else:
                counter = 0
                print(len(self.spectra), len(idx), len(self.fit_results))
                for ii, sp in enumerate(self.spectra):
                    if idx is None or idx[ii]:
                        sp.spectrum(microphase=sp.get_DC_phase()+np.pi/2+self.fit_results[counter].params["muphase"].value)
                        linewidths.append(sp.linewidth(mode="field")*np.sqrt(3))
                        counter +=1
        else:
            for ii, sp in enumerate(self.spectra):
                if idx is None or idx[ii]:
                    if method=="abs":
                        res = sp.absolute_line_fit()
                        linewidths.append(res.params["Hpp"].value*np.sqrt(3))
                    elif method=="complex":
                        res = sp.complex_line_fit(minHpp=minHpp, maxHpp=maxHpp)
                        linewidths.append(res.params["Hpp"].value*np.sqrt(3))
                    elif method=="fitpp":

                        res = sp.complex_line_fit(minHpp=minHpp, maxHpp=maxHpp)
                        sp.spectrum(microphase=sp.get_DC_phase()+np.pi/2+res.params["muphase"].value)
                        linewidths.append(sp.linewidth(mode="field")*np.sqrt(3))


        return np.array(linewidths)

    def cut_spectrum(self, start, end, mode="points"):
        for sp in self.spectra:
            sp.cut_spectrum(start, end, mode=mode)

    def plot_frequency_field_map(self, ax, fieldstitch=False):
        if fieldstitch:
            maxH = np.max(np.array([np.max(sp.field) for sp in self.spectra]))
            minH = np.min(np.array([np.min(sp.field) for sp in self.spectra]))


            dH = np.abs(self.spectra[0].field[1]-self.spectra[0].field[0])
            if int((maxH-minH)/dH)-(maxH-minH)/dH < 1e-6:
                L = int((maxH-minH)/dH)+2

            field = np.linspace(minH, maxH, L)
            Hfield = np.ones(len(self.spectra)).reshape((len(self.spectra), 1))*field

        else:
            maxcurr = np.max(np.array([np.max(sp.current) for sp in self.spectra]))
            mincurr = np.min(np.array([np.min(sp.current) for sp in self.spectra]))

            dcurr = np.abs(self.spectra[0].current[1]-self.spectra[0].current[0])

            L = int((maxcurr-mincurr)/dcurr)+1
            curr = np.linspace(mincurr, maxcurr, L)
            field = self.spectra[0].calibrationI(curr)
            Hfield = np.ones(len(self.spectra)).reshape((len(self.spectra), 1))*field

        frequencies = []

        absorption = []
        for ii, sp in enumerate(self.spectra):
            ab = np.zeros(L)
            if fieldstitch:
                idx1 = np.abs(field-np.min(sp.field)).argmin()
            else:

                idx1 = np.abs(curr-np.min(sp.current)).argmin()
                if curr[idx1] > np.min(sp.current):
                    idx1 -=1
            if sp.current[0] < sp.current[-1]:
                ab[idx1:idx1+len(sp.absorption)] = np.sqrt(sp.absorption**2+sp.dispersion**2)
            else:
                ab[idx1:idx1+len(sp.absorption)] = np.sqrt(sp.absorption**2+sp.dispersion**2)[::-1]

            absorption.append(ab)
            frequencies.append(L*[sp.res_freq])
        frequencies = np.array(frequencies)
        absorption = np.array(absorption)
        ax[0].contourf(Hfield, frequencies, absorption, cmap="jet", levels=100)
        absorption2 = absorption/np.max(absorption, axis=1).reshape(absorption.shape[0], 1)
        ax[1].contourf(Hfield, frequencies, absorption2, cmap="jet", levels=100)

        for a in ax:
            a.set_ylabel("Microwave frequency [GHz]")
            a.set_xlabel("Magnetic field [G]")
        return frequencies, absorption/np.max(absorption, axis=1).reshape(absorption.shape[0], 1), absorption

    def get_chisqr(self):
        chisq = []
        for res in self.fit_results:
            chisq.append(res.chisqr)
        return np.array(chisq)

    def linewidth_analysis(self, fmin, fmax, ffitmax, Hmin, Hmax, chisqthreshold=2.0, lwmin=1e-5, lwmax=100, f0guess=None):
        f = self.get_microwave_frequencies()
        H = self.get_resonance_field()
        A = np.logical_and(f > fmin, f < fmax)
        B = np.logical_and(f > fmin, f < ffitmax)
        dH = self.spectra[0].field[1]-self.spectra[0].field[0]
        a, b = np.polyfit(H[B], f[B], 1)


        linewidth = []
        resfreq = []
        chisqf = []
        Hcut = np.linspace(Hmin, Hmax, int((Hmax-Hmin)/dH)+1)
        for h in Hcut:
            if f0guess is None:
                f, disp, absorp, res, sp = self.frequency_cut_spectrum(h, lwmin=lwmin, lwmax=lwmax, f0guess=a*h+b)
            else:
                f, disp, absorp, res, sp = self.frequency_cut_spectrum(h, lwmin=lwmin, lwmax=lwmax, f0guess=f0guess)

            linewidth.append(res.params["Hpp"].value*np.sqrt(3))
            resfreq.append(res.params["H0"].value)
            chisqf.append(res.chisqr)
        linewidthfreq=np.array(linewidth)
        resfreq = np.array(resfreq)

        chisqf = np.array(chisqf)
        D = (chisqf-np.mean(chisqf))/np.mean(chisqf) < chisqthreshold
        linewidthfreq = linewidthfreq[D]
        resfreq = resfreq[D]

        fit_results, fitresulttmp, chisq, C = self.automatic_fit(idx=B)
        linewidthfield = np.array([res.params["Hpp"].value*np.sqrt(3) for res in fit_results])
        resfield = np.array([res.params["H0"].value for res in fit_results])

        a, b = np.polyfit(resfield, f[B][C], 1)
        return a, b, linewidthfreq, linewidthfield, A, B, C, D, chisq, chisqf, resfreq, resfield, Hcut


class TransmissionSpectrum:

    def __init__(self):
        self.current = None
        self.field = None
        self.frequency = None
        self.s = None

    def __sub__(self, y):

        newTS = TransmissionSpectrum()
        newTS.current = self.current
        newTS.field = self.field
        newTS.frequency = self.frequency


        self.s = self.normalized_transmission(normalize="edge") - y.normalized_transmission(normalize="edge")

        return newTS

    def load_hdf5(self, filename):

        with h5py.File(filename, "r") as f:
            self.frequency = np.array(f["Frequency"])
            self.current = np.array(f["Current"])
            self.field = np.array(f["Field"])
            self.s = np.zeros((len(self.current), len(self.frequency)), dtype=complex)

            for i in range(len(f["Traces"].keys())):

                self.s[i] = np.array(f["Traces/Trace{:d}".format(i)])
                #plt.plot(20*np.log10(np.abs(np.array(f["Traces"]["Trace{:d}".format(i)]))))

    def normalized_transmission(self, normalize="edge"):

        if normalize=="edge":
            return 20*np.log10(np.abs(self.s))-20*np.log10(np.abs(self.s[-1]))
        elif normalize=="center":
            idx = int(len(self.field)/2)
            return 20*np.log10(np.abs(self.s))-20*np.log10(np.abs(self.s[idx]))

    def plot_frequency_cut(self, ax, freqs, type="field"):

        T = self.normalized_transmission()
        for freq in freqs:
            idx = (np.abs(self.frequency - freq*1e9)).argmin()
            if type=="field":
                ax.plot(self.field, T[:, idx], label="{:2f} GHz".format(freq))
            elif type=="current":
                ax.plot(self.current, T[:, idx], label="{:2f} GHz".format(freq))

        if type=="field":
            ax.set_xlabel("Magnetic field [T]", fontsize=24)
        elif type=="current":
            ax.set_xlabel("Magnetic coil current [A]", fontsize=24)

        if len(freqs) == 1:
            ax.set_title("Cut at {:2f} GHz".format(freq), fontsize=24)
        else:
            ax.legend()
        ax.set_ylabel("Transmission [dB]")
        ax.grid()

    def plot_trace(self, ax, idx):
        ax.plot(self.frequency/1e9, 20*np.log10(np.abs(self.s[idx])))

    def contourplot(self, ax, fig, type="field", normalize="edge", stop=None):
        if stop is None:
            stop = len(self.frequency)
        if type=="current":
            F, C = np.meshgrid(self.frequency[:stop], self.current)
            ax.set_ylabel("Magnetic coil current [A]", fontsize=24)
        elif type=="field":
            F, C = np.meshgrid(self.frequency[:stop], self.field)
            C /= 1.01e4
            ax.set_ylabel("Magnetic field [T]", fontsize=24)
        im = ax.contourf(F/1e9, C, self.normalized_transmission(normalize=normalize)[:,:stop], cmap="jet_r", levels=100)
        ax.set_xlabel("Frequency [GHz]", fontsize=24)
        ax.tick_params(axis='both', which='major', labelsize=18)

        divider = make_axes_locatable(ax)
        cax = divider.append_axes('right', size='5%', pad=0.05)
        cbar = fig.colorbar(im, cax=cax, orientation='vertical')
        cbar.ax.set_title("S12 [dB]", fontsize=18)
