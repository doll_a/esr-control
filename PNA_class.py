# -*- coding: utf-8 -*-

import vxi11

import sys

import time
import struct
import numpy as np
import matplotlib.pyplot as plt

import os
import os.path




class VNA:
    def __init__(self, start=2.0, stop=6.0, points=1601, req_type='f'):
        # PNA address/name
        #adapter="129.129.131.142"
        # use hostname instead of IP
        adapter="K-N5244B-54190"


        self.inst=vxi11.Instrument(adapter)
        self.inst.open()

        print(self.inst.ask("*IDN?\r\n"))



        # init type and mode maps
        self.avail_modes = ['S11', 'S22', 'S12', 'S21']

        self.type_map = {}
        self.type_map['f'] = 'LIN'
        self.type_map['t'] = 'CW'

        self.IFBW_dt = np.loadtxt("..\esr-control\IFBW_dt.txt")

    def __del__(self):
        self.inst.close()

    def set_meas(self, mode):
        if not(mode in self.avail_modes):
            print('Problem setting the requested mode {:s}\nAvailable modes:'.format(mode))
            print(self.avail_modes)

            return

        self.inst.write('CALC:MEAS:PAR {:s};\r\n'.format(mode))

    def get_meas(self):

        mode_str = self.inst.ask('CALC:MEAS:PAR?;\r\n')

        return mode_str.replace('"','')


    def set_type(self, req_type):

        avail_types = list(self.type_map.keys())
        if not(req_type in avail_types):
            print('Problem setting the requested mode {:s}\nAvailable types:'.format(req_type))
            print(avail_types)

            return

        self.inst.write('SENS:SWE:TYPE {:s};\r\n'.format(self.type_map[req_type]))


    def get_type(self):


        ret_type = self.inst.ask('SENS:SWE:TYPE?;\r\n')

        return list(self.type_map.keys())[list(self.type_map.values()).index(ret_type)]

    def get_dwell_time(self):

        return self.inst.ask("SENS:SWE:DWEL?;\r\n")

    def set_CW(self, freq):
        self.inst.write('SENS:FREQ {:e};\r\n'.format(freq*1e9))


    def set_xaxis(self, start, stop, points):

        if self.get_type() == 'f':
            self.inst.write('SENSe:FREQuency:STARt {:e};\r\n'.format(start*1e9))
            self.inst.write('SENSe:FREQuency:STOP {:e};\r\n'.format(stop*1e9))
            self.inst.write('SENSe:SWEep:POINts {:e};\r\n'.format(points))
        else:
            # self.inst.write('SENS:SWE:TIME:STAR {:f};\r\n'.format(start)) # not supported
            self.inst.write('SENSe:SWEep:POINts {:e};\r\n'.format(points))
            self.inst.write('SENS:SWE:TIME {:f};\r\n'.format(stop))

    def get_xaxis(self):

        if self.get_type() == 'f':
            self.start = float(self.inst.ask('SENSe:FREQuency:STARt?;\r\n'))
            self.stop = float(self.inst.ask('SENSe:FREQuency:STOP?;\r\n'))
            self.points = int(float(self.inst.ask('SENSe:SWEep:POINts?;\r\n')))

        else:
            #self.start = float(self.inst.ask('SENS:SWE:TIME:STAR?;\r\n')) # noo longer supprted
            self.start = 0
            self.stop = float(self.inst.ask('SENS:SWE:TIME?;\r\n'))
            self.points = int(float(self.inst.ask('SENSe:SWEep:POINts?;\r\n')))

        return np.linspace(self.start, self.stop, self.points)

    def set_points(self, points):
        self.inst.write('SENSe:SWEep:POINts {:e};\r\n'.format(points))

    def auto_sweep_time_on(self):
        self.inst.write('SENSe:SWEep:time:auto on;\r\n')

    def auto_sweep_time_off(self):
        self.inst.write('SENSe:SWEep:time:auto off;\r\n')

    def get_sweep_time(self):
        return float(self.inst.ask('SENS:SWE:TIME?;\r\n'))

    def set_BW(self, BW):
        self.inst.write('SENS:BWID {:e};\r\n'.format(BW))

    def get_BW(self):
        return float(self.inst.ask('SENS:BWID?;\r\n'))

    def set_power(self, power):
        minp = float(self.inst.ask("SOURce:POWer? Min;\r\n"))
        maxp = float(self.inst.ask("SOURce:POWer? Max;\r\n"))
        if power >= minp and power <= maxp:
            self.inst.write("SOUR1:POW1:LEV {:f};\r\n".format(power))
        else:
            print("Your power is not within the allowed range ({:f} dBm to {:f} dBm).".format(minp, maxp))

    def get_power(self):
        return float(vna.inst.ask("SOUR1:POW1:LEV?;\r\n"))

    def set_autoscale(self):
        self.inst.write("DISP:MEAS:Y:AUTO;\r\n")

    def set_trigger(self):
        self.inst.write("TRIG:SOUR EXT;\r\n")
        self.inst.write("TRIG:TYPE EDGE;\r\n")
    # method to retrieve the data
    def get_trace(self):

        cmd = 'CALC:MEAS:DATA:SDATA?;\r\n'

        self.inst.write('FORMat:DATA REAL,32;\r\n')
        # self.inst.write('FORMat:DATA ASCii,0;\r\n')

        # perform the measurement
        self.inst.write('SENS:SWE:MODE SING;\r\n')

        self.inst.write('INITiate:IMMediate;*wai;\r\n')
# ""


        # ask raw created a timeout at larger sizes
        # data = self.inst.ask_raw(cmd.encode())

        self.inst.write(cmd)
        time.sleep(0.25)

        try:
            data = self.inst.read_raw()
        except:
            # very long data acquisitions might end here
            # there might be a more elegant solution using a status bit...!
            # for now, let's just make a total of 1 minute
            data_read = False
            print('This seems to be along scan! Allowing for 60 seconds to read out data')
            for ii in range(12):
                if data_read:
                    continue
                time.sleep(5)
                try:
                    data = self.inst.read_raw()
                    data_read = True
                    print('Long scan finished')
                except:
                    pass

            if (data_read == False):
                raise Exception('No data retrieved from VNA after 60 seconds!')



        # check the first identifier
        flag = struct.unpack_from('>c',data, 0)[0].decode()
        if flag != '#':
            print('Data Flag not received! Problem!')

        n_dig = int(struct.unpack_from('>c',data, 1)[0])
        n_bytes = 0

        # now get the byte count n_dig times
        for ii_dig in range(n_dig):
            n_bytes += 10**(n_dig-ii_dig-1) * int(struct.unpack_from('>c',data, 2+ii_dig)[0])

        fmt = '>{:d}f'.format(n_bytes//4)
        s_pairs = struct.unpack_from(fmt, data, 2+n_dig)
        s = np.empty(len(s_pairs)//2, 'complex')
        s.real, s.imag = s_pairs[::2], s_pairs[1::2]

        # keep on running
        self.inst.write('SENS:SWE:MODE CONT;\r\n')

        self.s_x = self.get_xaxis()
        self.s_y = s

        return s

    def save_trace(self, folder="./vna_meas/", fileending=""):
        tstamp = time.strftime("%Y%m%d_%H%M%S")

        #fname = '{}_{:.1f}K_{:.1f}G{:d}'.format(tstamp,temp,field, number)
        #fname = fname.replace('.','p')
        fname = folder + tstamp + fileending

        if os.path.isdir(folder) == False:
            os.mkdir(folder)

        self.s_x = self.get_xaxis()
        s = self.get_trace()
        self.s_y = s

        np.savez(fname, s_x = self.s_x, s_y = self.s_y)

        print('Stored under: '+fname)
        return s

    def plot_last(self, fignum = 1):

        if hasattr(self,'s_x'):
            plt.figure(fignum)
            plt.clf()
            plt.plot(self.s_x, abs(self.s_y))

    def find_min_freq(self):
        self.inst.write("CALC1:MEAS:MARK1 ON;\r\n")
        self.inst.write('CALC1:MEAS:MARK1:FUNC:EXEC MIN;\r\n')
        return float(self.inst.ask("CALC1:MEAS:MARK1:X?;\r\n"))/1e9

    def save_time_trace(self, end, points, IFBW, **keywords):
        self.set_type("t")
        self.set_BW(IFBW)
        self.set_xaxis(0, end, points)
        self.save_trace(**keywords)

    def find_trace_settings(self, res, points_per_cycle, center_freq):

        T = 1/res
        dt = 1/(center_freq*points_per_cycle)

        idx = np.abs(self.IFBW_dt[:,1]-dt).argmin()
        realdt = self.IFBW_dt[idx,1]

        return np.ceil(T/realdt), self.IFBW_dt[idx,0]

    def set_time_trace(self, TT):

        settings = {}
        if TT[0] == "cycles":
            settings["points per cycle"] = TT[2]
            settings["cycle num"] = TT[3]
            settings["IF_BW"] = int(TT[2]*TT[1]*0.922)
            print("IF BW: {:f} Hz".format(settings["IF_BW"]))

            self.set_BW(settings["IF_BW"])
            self.set_xaxis(0, TT[3]/TT[1], int(TT[3]*TT[2]))

        elif TT[0] == "time":
            #1 Freq
            #2 Time
            #3 points
            #4 IFBW
            self.auto_sweep_time_off()
            settings["cycle num"] = TT[2]*TT[1]
            settings["points per cycle"]  = TT[3]/settings["cycle num"]
            settings["IF_BW"] = TT[4]

            if settings["points per cycle"] < 2:
                print("Your sampling {:f} points per cycle. That's below the Nyquist limit!".format(settings["points per cycle"]))
            else:
                print("Your sampling {:f} points per cycle.".format(settings["points per cycle"]))

            if settings["IF_BW"] < TT[1]:
                print("Warning: The IF bandwidth is below the modulation frequency.")

            #if  0.972/settings["IF_BW"]> TT[2]/TT[3]:
            #    print("Warning: The IF bandwidth is too low for the choosen sample rate. The VNA will overwrite the sampling time.")

            self.set_BW(settings["IF_BW"])
            self.set_xaxis(0, TT[2], TT[3])

        elif TT[0] == "points":
            #1 points
            # 2 IFBW
            self.auto_sweep_time_on()
            self.set_BW(TT[2])
            self.set_points(TT[1])

        elif TT[0] == "resolution":
            self.auto_sweep_time_on()
            p, settings["IF_BW"] = self.find_trace_settings(TT[2], TT[3], TT[1])
            print("IF BW: {:f} Hz".format(settings["IF_BW"]))
            print("Number of points {:f}".format(p))
            self.set_BW(settings["IF_BW"])
            self.set_points(p)

        return settings

    def noise_measurement(self, res, maxfreq, **keywords):
        self.set_type("t")
        self.set_time_trace(["resolution", maxfreq, res, 2])
        self.save_trace(**keywords)

if __name__ == '__main__':
    vna = VNA()
