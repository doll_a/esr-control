import PowerSupplyCaylarLib
import PNA_class
import time
import sys
import numpy as np
import h5py
import datetime
import pandas as pd
from mercuryitc import MercuryITC
from matplotlib.gridspec import GridSpec
sys.path.insert(0, './caylar_class')


class ESRExperimentControl:

    def __init__(self, ps_ip, ps_port, itc_ip):

        self.vna = PNA_class.VNA()
        self.ps =PowerSupplyCaylarLib.CaylarPowerSupply(2)
        self.ps.connect(ps_ip, ps_port)

        self.params = {"IF_BW":np.nan, "Material":"", "Resonance frequency": np.nan,
                        "Temperature":np.nan, "Modulation frequency": np.nan,
                        "Modulation ppA": np.nan, "Power level": np.nan, "Spectrum type": "",
                        "Static field angle": np.nan, "Resonator type":"", "Magnetic field": np.nan,
                        "Measurement type": None}

        try:
            self.itc =  MercuryITC(itc_ip).modules[-1]
        except:
            self.itc = None
            print("Couldn't connect to ITC. No temperatures will be recorded.")

        self.fig = None
        self.display = None
        self.axTT = None
        self.axB = None
        self.axT = None
        self.axS = None

    def initialize_fig(self, fig, display):
        self.fig = fig
        self.display = display
        gs = GridSpec(2, 4, figure=fig)
        self.axTT = fig.add_subplot(gs[0, :2])
        self.axTT.set_xlabel("Time [s]")
        self.axTT.set_ylabel("S11")
        self.axTT.grid()

        self.axB = fig.add_subplot(gs[1,0])
        self.axB.set_ylabel("Magnetic field [G]")
        self.axB.grid()

        self.axT = fig.add_subplot(gs[1, 1])
        self.axT.set_ylabel("Temperature [K]")
        self.axT.grid()

        self.axS = fig.add_subplot(gs[:, 2:])

    def update_plot(self, t, T, B, S):
        if self.fig is not None:
            self.axTT.lines[0].set_ydata(np.abs(S))
            self.axTT.relim()
            self.axTT.autoscale()
            self.axT.scatter(t, T, color="k")
            self.axB.scatter(t, B, color="k")
            #self.fig.canvas.draw()
            #self.fig.canvas.flush_events()
            self.display.update(self.fig)

    def find_resonance(self, coilc, fstart, fstop):
        self.ps.setCurrent(coilc)
        self.vna.set_type("f")
        self.vna.set_xaxis(fstart, fstop, 5000)
        time.sleep(0.1)
        minfreq = self.vna.find_min_freq()
        self.params["Resonance frequency"] = minfreq
        return minfreq

    def setup_CW(self):
        self.vna.set_type("t")
        self.vna.set_CW(self.params["Resonance frequency"])
        self.vna.set_xaxis(0, 2, 2000)

    def set_parameter(self, params):
        for k in params.keys():
            if k in self.params.keys():
                self.params[k] = params[k]

    def field_sweep(self, start, stop, spacing, n_rep=1, ROI=None, folder="./vna_meas/", default=True, res=1, points_per_cycle=10, magnetmode=False):

        self.params["Magnetic field"] = np.nan
        self.params["Spectrum type"] = "Modulated field sweep"

        self.vna.set_type("t")
        self.vna.set_trigger()

        print(self.params["Measurement type"])
        self.vna.set_meas(self.params["Measurement type"])

        settings = self.vna.set_time_trace(["resolution", self.params["Modulation frequency"], res, points_per_cycle])
        self.set_parameter(settings)

        if default:
            self.params["Power level"] = 0
            print("Power level: {:d} dBm".format(self.params["Power level"]))
            if self.itc is not None:
                self.params["Temperature"] = self.itc.temp[0]

        self.vna.set_power(self.params["Power level"])

        if self.params["Resonance frequency"] is not None:
            print("Setting Resonance frequency")
            self.vna.set_CW(self.params["Resonance frequency"])

        if self.fig is not None:
            t = self.vna.get_xaxis()
            #self.fig.clf()
            self.axTT.plot(t, np.ones(len(t)))
            self.display.update(self.fig)

        if ROI is not None:
            field_vec1 = np.linspace(start, ROI[0], int((ROI[0]-start)/spacing)+1)
            field_vec2 = np.linspace(ROI[0]+ROI[2], ROI[1], int((ROI[1]-ROI[0]-ROI[2])/ROI[2])+1)
            field_vec3 = np.linspace(ROI[1]+spacing, stop, int((-ROI[1]+stop-spacing)/spacing)+1)
            field_vec = np.concatenate([field_vec1, field_vec2, field_vec3])
        else:
            field_vec = np.linspace(start, stop, int(abs(stop-start)/spacing)+1)

        if magnetmode:
            print(field_vec)
            field_vec = self.ps.calibrationB(field_vec)
            print(field_vec)
        # turn on in case it is off

        power_state = int(self.ps.getPowerState().split(" ")[1])
        if (power_state == 0):
            self.ps.setPowerOn()
        self.ps.setCurrent_wait(field_vec[0])

        #%% activate field modulation
        self.ps.setModFreq(self.params["Modulation frequency"])
        self.ps.setModCurrent(self.params["Modulation ppA"])
        self.ps.setModOn()


        #%% run the fieldsweep
        filenames = []
        for ii in range(n_rep):
            tstamp = time.strftime("%Y%m%d_%H%M%S")
            filenames.append("{:s}Spectrum_{:s}.hdf5".format(folder, tstamp))
            with h5py.File("{:s}Spectrum_{:s}.hdf5".format(folder, tstamp), "w") as f:

                f["Current"] = field_vec
                for k in self.params.keys():
                    f[k] = self.params[k]

                Bfield = np.zeros_like(field_vec)
                HallT = np.zeros_like(field_vec)
                ColdfingerT = np.zeros_like(field_vec)

                for i, field in enumerate(field_vec):

                    self.ps.setCurrent(field)
                    Bfield[i] = self.ps.getField_float()
                    HallT[i] = self.ps.getHallTemp_float()
                    if self.itc is not None:
                        ColdfingerT[i] = self.itc.temp[0]
                    else:
                        ColdfingerT[i] = self.params["Temperature"]

                    f["Traces/Trace{:d}/t".format(i)] = self.vna.get_xaxis()
                    s = self.vna.get_trace()
                    f["Traces/Trace{:d}/s".format(i)] = s
                    #self.update_plot(i, ColdfingerT[i], Bfield[i], s)
                    #f["Traces/Trace{:d}/s".format(i)] = vna.get_trace()

                f["Field"] = Bfield
                f["Hall temperature"] = HallT
                if self.itc is not None:
                    f["Cold finger temperature"] = ColdfingerT

            self.add_spectra(filenames[-1])

        return filenames

    def box_field_sweep(self, Hmin, Hmax, spacingH, dH, fmin, fmax, spacingf, df, n_rep=1, ROI=None, folder="./vna_meas/", default=True, res=1, points_per_cycle=10):

        a = (Hmax-Hmin)/(fmax-fmin)
        b = Hmin-a*fmin

        Hvec = np.linspace(Hmin, Hmax, int(abs(Hmax-Hmin)/spacingH)+1)
        for rf in np.linspace(fmin, fmax, int(abs(fmax-fmin)/spacingf)+1):
            self.params["Resonance frequency"] = rf
            print(rf)
            Hctmp = a*rf + b
            Hc = Hvec[np.argmin(np.abs(Hvec-Hctmp))]

            dHf = min([(rf-fmin)*a, df*a])
            dHround = int(max([dH, dHf])/spacingH)*spacingH
            self.field_sweep(Hc-dHround, Hc+dHround, spacingH, n_rep=n_rep, ROI=ROI, folder=folder, res=res, default=default, points_per_cycle=points_per_cycle, magnetmode=True)

        for rf in np.linspace(fmin-df, fmin-spacingf, int(abs(df)/spacingf)):
            self.params["Resonance frequency"] = rf
            print(rf)
            dH = int((rf-fmin+df)*a/spacingH)*spacingH
            self.field_sweep(Hmin, Hmin+dH, spacingH, n_rep=n_rep, ROI=ROI, folder=folder, res=res, default=default, points_per_cycle=points_per_cycle, magnetmode=True)

        for rf in np.linspace(fmax+spacingf, fmax+df, int(abs(df)/spacingf)):
            self.params["Resonance frequency"] = rf
            print(rf)
            dH = int((-rf+fmax+df)*a/spacingH)*spacingH
            self.field_sweep(Hmax-dH, Hmax, spacingH, n_rep=n_rep, ROI=ROI, folder=folder, res=res, default=default, points_per_cycle=points_per_cycle, magnetmode=True)

    def transmission(self, startf, stopf, startH, stopH, HN, folder="./vna_meas/", default=True):

        if default:
            self.params["IF_BW"] = 1e5

        self.vna.set_BW(self.params["IF_BW"])

        self.params["Resonance frequency"] = np.nan
        self.params["Magnetic field"] = np.nan
        self.params["Spectrum type"] = "Transmission"
        self.params["Modulation ppA"] = 0
        self.params["Modulation frequency"] = 0

        self.vna.set_xaxis(startf, stopf, 5000)

        field_vec = np.linspace(startH, stopH, HN)
        self.ps.setCurrent_wait(field_vec[0])
        time.sleep(np.abs(field_vec[0]*1.1))

        freq_vec = self.vna.get_xaxis()
        tstamp = time.strftime("%Y%m%d_%H%M%S")
        filename = "{:s}Spectrum_{:s}.hdf5".format(folder, tstamp)

        with h5py.File(filename, "w") as f:
            for k in self.params.keys():
                f[k] = self.params[k]

            f["Current"] = field_vec
            f["Frequency"] = freq_vec

            Bfield = np.zeros_like(field_vec)
            HallT = np.zeros_like(field_vec)
            ColdfingerT = np.zeros_like(field_vec)

            for i, H in enumerate(field_vec):
                self.ps.setCurrent_wait(H)
                Bfield[i] = self.ps.getField_float()
                HallT[i] = self.ps.getHallTemp_float()
                f["Traces/Trace{:d}".format(i)] = self.vna.get_trace()

            f["Field"] = Bfield
            f["Hall temperature"] = HallT

        self.add_spectra(filename)
        self.fig.clf()

    def add_spectra(self, filename):
        newentry = self.params.copy()
        newentry["Date"] = datetime.datetime.now()
        newentry["Filename"] = filename
        newentry["Good spectra"] = False

        spectra = pd.read_excel("..\esr-control\Spectra.xlsx")
        spectra = spectra.append(newentry, ignore_index=True)
        spectra.to_excel("..\esr-control\Spectra.xlsx", index=False)

    def plot_temperature(self, t):
        self.axT.clear()
        self.axT.grid()
        for i in range(t):
            self.axT.scatter(i, self.itc.temp[0], color="k")
            self.display.update(self.fig)
            time.sleep(1)
