#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 20 14:54:41 2021

@author: pi
"""


import numpy as np
import matplotlib.pyplot as plt
import os



tstamps = []
exclude_idx = []
labels = []
paths = []

tstamps.append([('121044','121648')]);                  labels.append('test');           exclude_idx.append([]);    paths.append('vna_meas')


# data containers
s_x_c = []
s_y_c = []

# iterate over datasets
for ii_set, tstamp in enumerate(tstamps):
    
    # get files
    allfiles = [f for f in os.listdir(paths[ii_set]) if (len(f) > 15 and f[8] == '_' and '.npz' in f)]
    
    files = []
    for tstamp_item in tstamp:
        for file in allfiles:
            currstamp = int(file[0:8])*1e6 + int(file[9:15])
            # check if the actual timestamp is part of the set
            # 1. specification as tuple: range of timestamps
            if (type(tstamp_item) == tuple):
                strtstamp_str = tstamp_item[0].replace('_','')
                strtstamp_len = len(strtstamp_str)
                strtstamp = int(strtstamp_str)
                
                endstamp_str = tstamp_item[1].replace('_','')
                endstamp_len = len(endstamp_str)
                endstamp = int(endstamp_str)
                
                cmpstamp_strt = np.mod(currstamp,10**strtstamp_len)
                cmpstamp_end = np.mod(currstamp,10**endstamp_len)
                
                if ((cmpstamp_strt >= strtstamp) and ( cmpstamp_end <= endstamp)):
                    files.append(file)
                                
            else:
                tgtstamp_str = tstamp_item.replace('_','')
                tgtstamp_len = len(tgtstamp_str)
                tgtstamp = int(tgtstamp_str)
                    
                cmpstamp = np.mod(currstamp,10**tgtstamp_len)
                                
                if (cmpstamp == tgtstamp):
                    files.append(file)
         
    # sort the files according to their timestamps
    files.sort(key = lambda x: int(x[0:8])*1e6 + int(x[9:15]))

    # get all data
    s_x = []
    s_y = []
    temp = []
    field = []
    valid = []
    ignored = []
    for ii,file in enumerate(files):
        
        fullpath = os.path.join(paths[ii_set],file)
        
        loadz = np.load(fullpath)
        
        
        s_x.append(loadz['s_x'])
        s_y.append(loadz['s_y'])
        temp.append(loadz['temp'])
        field.append(loadz['field'])
        
        
        valid.append(ii)
        if (ii in exclude_idx[ii_set]):
            s_x.pop()
            s_y.pop()
            valid.pop()
            ignored.append(ii)
    
    
    # plot data
    #figs = []
    #axarrs = []
    fignum = 10 + ii_set
    plt.close(fignum)
    plt.figure(fignum)
    plt.clf()
    offset  = 0.1
    for ii, dta in enumerate(s_y):
        plt.plot(s_x[ii], abs(s_y[ii]) + ii*offset)
        