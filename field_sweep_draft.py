# -*- coding: utf-8 -*-

#caylar magnet
import sys
sys.path.insert(0, './caylar_class')
import importlib

import PowerSupplyCaylarLib
import SmallController

import PNA_class
importlib.reload(PNA_class)

import numpy as np
import matplotlib.pyplot as plt

import time
import h5py

def field_sweep(ps, vna, start, stop, spacing, uw_freq, modfreq=10, modcurr=0.01, IF_BW=1e3, pow_lev=-15, n_rep=1, ROI=None, folder="./vna_meas/", material="BDPA", temperature=300):

    if ROI is not None:
        field_vec1 = np.linspace(start, ROI[0], int((ROI[0]-start)/spacing)+1)
        field_vec2 = np.linspace(ROI[0]+ROI[2], ROI[1], int((ROI[1]-ROI[0])/ROI[2])+1)
        field_vec3 = np.linspace(ROI[1]+spacing, stop, int((-ROI[1]+stop)/spacing)+1)
        field_vec = np.concatenate([field_vec1, field_vec2, field_vec3])
    else:
        field_vec = np.linspace(start, stop, int(abs(stop-start)/spacing)+1)

    # turn on in case it is off

    power_state = int(ps.getPowerState().split(" ")[1])
    if (power_state == 0):
        ps.setPowerOn()
    ps.setCurrent(field_vec[0])
    time.sleep(np.abs(field_vec[0]*1.1))
    #%% activate field modulation

    ps.setModFreq(modfreq)
    ps.setModCurrent(modcurr)
    ps.setModOn()


    #%% turn on the PNA
    if uw_freq is not None:
        vna.set_CW(uw_freq)

    vna.set_BW(IF_BW)

    vna.set_power(pow_lev)
    #what is the minimum time again?
    vna.set_xaxis(0,50/modfreq, 2000)

    #Lock in to the signal

    #%% run the fieldsweep
    filenames = []
    for ii in range(n_rep):
        tstamp = time.strftime("%Y%m%d_%H%M%S")
        filenames.append("{:s}Spectrum_{:s}.hdf5".format(folder, tstamp))
        with h5py.File("{:s}Spectrum_{:s}.hdf5".format(folder, tstamp), "w") as f:

            f["Current"] = field_vec


            Bfield = np.zeros_like(field_vec)
            HallT = np.zeros_like(field_vec)

            for i, field in enumerate(field_vec):

                ps.setCurrent(field)
                Bfield[i] = ps.getField_float()
                HallT[i] = ps.getHallTemp_float()

                f["Traces/Trace{:d}/t".format(i)] = vna.get_xaxis()
                f["Traces/Trace{:d}/s".format(i)] = vna.get_trace()
                #f["Traces/Trace{:d}/s".format(i)] = vna.get_trace()


            f["Field"] = Bfield
            f["IF_BW"] = IF_BW
            f["Material"] = material
            f["Resonance frequency"] = uw_freq
            f["Temperature"] = temperature
            f["Modulation frequency"] = modfreq
            f["Modulation ppA"] = modcurr
            f["Power level"] = pow_lev
            f["Hall temperature"] = HallT

    return filenames
