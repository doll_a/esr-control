from scipy.constants import physical_constants
import numpy as np
gamel = physical_constants["electron gyromag. ratio in MHz/T"][0]/1000


from scipy.optimize import fsolve, minimize
from lmfit import Model, Parameter, Parameters, Minimizer

class FMR_biaxial_model:

    def __init__(self, Kx, Kz, Ms=1, g=2.002, T2=0):

        self.Ms = Ms
        self.gam = gamel*g/2.002
        self.Kx = Kx #J/m^3
        self.Kz = Kz #J/m^3
        self.T2 = T2
        self.fitres  = None

    def free_resonance_frequency(self, H, theta_h, phi_h):
        theta_m, phi_m = self.magnetisation_angle(H, theta_h, phi_h)
        Epp = 2*self.Kx*np.sin(theta_m)**2*(np.cos(phi_m)**2-np.sin(phi_m)**2) + H*np.sin(theta_m)*np.sin(theta_h)*np.cos(phi_m-phi_h)*self.Ms

        Ett = -2*self.Kx*np.cos(phi_m)**2*(np.cos(theta_m)**2-np.sin(theta_m)**2)-2*self.Kz*(np.cos(theta_m)**2-np.sin(theta_m)**2)+H*(np.sin(theta_m)*np.sin(theta_h)*np.cos(phi_m-phi_h)+np.cos(theta_m)*np.cos(theta_h))*self.Ms
        Etp = 2*self.Kx*np.sin(theta_m)*np.cos(theta_m)*np.sin(phi_m)*np.cos(phi_m)+H*np.cos(theta_m)*np.sin(theta_h)*np.sin(phi_m-phi_h)

        return self.gam/(self.Ms*np.sin(theta_m))*np.sqrt(Epp*Ett-Etp**2)

    def free_resonance_frequency_np(self, H, theta_h, phi_h):
        om = np.zeros_like(H)
        for i, h in enumerate(H):
            om[i] = self.free_resonance_frequency(h, theta_h, phi_h)
        return om

    def magnetisation_energy(self, x, phi, H, theta_h, phi_h):
        return -self.Kx*np.sin(x)**2*np.cos(phi)**2 + self.Kz*np.cos(x)**2-H*(np.sin(x)*np.sin(theta_h)*np.cos(phi-phi_h)+np.cos(x)*np.cos(theta_h))*self.Ms


    def magnetisation_angle(self, H, theta_h, phi_h, phi0=False):
        if phi0:
            f = lambda x: self.magnetisation_energy(x, 0, H, theta_h, 0)
            return minimize(f, np.pi/2, bounds=[(-0.001, np.pi/2)]).x[0]
        else:
            f = lambda x: self.magnetisation_energy(x[0], x[1], H, theta_h, phi_h)
            return minimize(f, np.array([np.pi/2, 0.1]), bounds=[(-0.001, np.pi/2), (0, np.pi)]).x

class FMR_uniaxial_model:

    def __init__(self, Kz, Ms=1, g=2.002):

        self.Ms = Ms
        self.gam = gamel*g/2.002
        self.Kz = Kz #J/m^3

    def magnetisation_energy(self, x, H, theta_h):
        return self.Kz*np.cos(x)**2-H*np.cos(x-theta_h)


    def mag_cond(self, x, H, theta_h):
        return 2*H*np.sin(x-theta_h)-2*self.Kz*np.sin(2*x)

    def magnetisation_angle(self, H, theta_h, method=0):

        if method==0:
            f = lambda x: self.mag_cond(x, H, theta_h)
            return fsolve(f, 0)[0]
        else:
            f = lambda x: self.magnetisation_energy(x, H, theta_h)
            return minimize(f, np.pi/2, bounds=[(-0.001, np.pi/2)]).x[0]


    def free_resonance_frequency(self, H, theta_h):
        theta_M = self.magnetisation_angle(H, theta_h, method=1)
        H1 = H*np.cos(theta_M-theta_h)-2*self.Kz*np.cos(theta_M)**2
        H2 = H*np.cos(theta_M-theta_h)-2*self.Kz*np.cos(2*theta_M)
        return self.gam*np.sqrt(H1*H2)

    def free_resonance_frequency_np(self, H, theta_h):
        om = np.zeros_like(H)
        for i, h in enumerate(H):
            om[i] = self.free_resonance_frequency(h, theta_h)
        return om

def fit_model_bi(H, f, theta_h, linewidth=None, Kxguess=None, Kzguess=None, theta0=None, varyt=False):
    if linewidth is None:
        linwidth = np.zeros_like(H)
    params = Parameters()
    params["Kz"] = Parameter(name='Kz', value=Kzguess)
    params["Kx"] = Parameter(name='Kx', value=Kxguess)

    if theta0 is not None:
        params["theta_0"] = Parameter(name="theta_0", value=theta0, vary=varyt)
    else:
        params["theta_0"] = Parameter(name="theta_0", vary=False, value=0)

    def residual(pars, data=None, eps=None):
    # unpack parameters: extract .value attribute for each parameter
        parvals = pars.valuesdict()
        mod = FMR_biaxial_model(parvals["Kx"], parvals["Kz"])

        residuals = np.array([])

        for ii in range(len(H)):
            if linewidth is None:
                restmp = mod.free_resonance_frequency_np(H[ii]/1e4, np.abs((theta_h[ii]-parvals["theta_0"])/180*np.pi), 0)-f[ii]
            else:
                restmp = np.sqrt(mod.free_resonance_frequency_np(H[ii]/1e4, np.abs((theta_h[ii]-parvals["theta_0"])/180*np.pi), 0)**2+0.25*linewidth[ii]**2)-f[ii]

            residuals = np.concatenate([residuals, restmp])

        return residuals

    minob = Minimizer(residual, params)
    res = minob.minimize()

    return res

def fit_model(H, f, theta_h, linewidth=None, Kzguess=None, theta0=None, varyt=False):
    if linewidth is None:
        linwidth = np.zeros_like(H)
    params = Parameters()
    params["Kz"] = Parameter(name='Kz', value=Kzguess)
    if theta0 is not None:
        params["theta_0"] = Parameter(name="theta_0", value=theta0, vary=varyt)
    else:
        params["theta_0"] = Parameter(name="theta_0", vary=False, value=0)

    def residual(pars, data=None, eps=None):
    # unpack parameters: extract .value attribute for each parameter
        parvals = pars.valuesdict()
        mod = FMR_uniaxial_model(parvals["Kz"])

        residuals = np.array([])

        for ii in range(len(H)):
            if linewidth is None:
                restmp = mod.free_resonance_frequency_np(H[ii]/1e4, np.abs((theta_h[ii]-parvals["theta_0"])/180*np.pi))-f[ii]
            else:
                restmp = np.sqrt(mod.free_resonance_frequency_np(H[ii]/1e4, np.abs((theta_h[ii]-parvals["theta_0"])/180*np.pi))**2+0.25*linewidth[ii]**2)-f[ii]

            residuals = np.concatenate([residuals, restmp])

        return residuals

    minob = Minimizer(residual, params)
    res = minob.minimize()

    return res


"""
    def precession_frequency_xaxis(h, theta_h, Kx, Kz, Ms=1):

        theta_m = find_magnetisation_angle_biaxial(h, theta_h, 0, Kx, Kz, phi0=True)
        return gamel*np.sqrt(2*Kx*(h*np.cos(theta_m-theta_h)*Ms-2*(np.cos(theta_m)**2-np.sin(theta_m)**2)*(Kx+Kz)))

    def precession_frequency_analytical(h, Kx, Kz, Ms=1):
        Hs = 2*(Kx+Kz)/Ms
        om = np.zeros_like(h)
        om[h < Hs] = gamel*np.sqrt(Kx*(Kx+Kz)*((2*(Kx+Kz)/Ms)**2-h[h<Hs]**2))/(Kx+Kz)
        om[h > Hs] = gamel*np.sqrt((h[h>Hs]-Hs)*(h[h>Hs]-2*Kz/Ms))
        return om

    def precession_frequency_high_field(H, theta_h, phi_h, Kx, Kz, Ms=1):
        return gamel*(H + (2*Kx*(np.cos(phi_h)**2-np.sin(phi_h)**2) -2*(Kx*np.cos(phi_h)**2+Kz)*(np.cos(theta_h)**2-np.sin(theta_h)**2))/(2*Ms))

    def precession_frequency_high_field_along_x(H, theta_h, Kx, Kz, Ms=1):
        return gamel*(H - (2*(Kx+Kz)*(np.cos(theta_h)**2-np.sin(theta_h)**2)-2*Kx+2*(Kx+Kz)*np.cos(theta_h)**2)/(2*Ms))

    def fit_model(H, f, theta_h):
        params = Parameters()
        params["phi_h"] = Parameter(name='phi_h', value=90/180*np.pi, max=np.pi, min=0)
        params["Kx"] = Parameter(name='Kx', value=0.02889983579638752*0.4, max=0.1, min=0)
        params["Kz"] = Parameter(name='Kz', value=0.37110016420361247, max=10, min=0)

        def residual(pars, data=None, eps=None):
        # unpack parameters: extract .value attribute for each parameter
            parvals = pars.valuesdict()


            residuals = np.array([])

            for ii in range(len(H)):
                restmp = self.precession_frequency_np(H[ii]/1e4, theta_h[ii]/180*np.pi, parvals["phi_h"], parvals["Kx"], parvals["Kz"])-f[ii]
                residuals = np.concatenate([residuals, restmp])
            print(np.sum(residuals**2))
            return residuals

        minob = Minimizer(residual, params)
        res = minob.minimize()

        return res

"""
