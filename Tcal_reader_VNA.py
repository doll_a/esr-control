# -*- coding: utf-8 -*-
"""
Created on Thu Oct 27 14:03:49 2022

@author: doll_a
"""
# -*- coding: utf-8 -*-

# reads temperature from two ITCs

import mercuryitc
import time

import matplotlib.pyplot as plt
import numpy as np


import PNA_class


m = mercuryitc.MercuryITC("TCPIP0::129.129.131.133::7020::SOCKET")


tmp = m.modules[-1]


# VNA pars

f_init = 8.25
f_stop = 15.25

temperature = tmp
field = 0



# vna class instance
vna =PNA_class.VNA(f_init, f_stop, points=10000)
vna.save_trace(temp=temperature,field=field)

last_temp = temperature
T_interval = 10



writefile = True
wrt_interval = 10
wrt_filename = time.strftime("%Y%m%d_%H%M") + '_tempdata.npz'



rd_interval = 3


plt_update = 2
plt_len = 100

dta = []

ii = 0

while True:
    
    ts = time.time()
        
    
    # meas resistance
    R = tmp.res[0]
    T = tmp.temp[0]
    
    # ref values
    # T_ref = tmp_ref.temp[0]
    # R_ref = tmp_ref.res[0]
    
    
    # dta.append([ts, T, R, T_ref, R_ref])
    dta.append([ts, T, R])
    
    # time.sleep(rd_interval)
    
    ii += 1
    
    # check if Temperature changed according to interval
    if (abs(T - last_temp) > T_interval):
        vna.save_trace(temp=T, field=field)
        
        last_temp = T

        
    
    # eventually update the T curve
    if ((ii % plt_update) == 0):
        
        pltdta = np.array(dta[-plt_len:])
        
        plt.figure(1)
        plt.clf()
        plt.subplot(211)
        plt.plot(pltdta[:,0]-pltdta[-1,0], pltdta[:,2],'o')
        plt.xlabel('Timestamp [s]')
        plt.ylabel('$R$ [$\Omega$]')
        
        
        plt.subplot(212)
        plt.plot(pltdta[:,0]-pltdta[-1,0], pltdta[:,1],'o')
        plt.xlabel('Timestamp [s]')
        plt.ylabel('$T$ [K]')
        
        plt.pause(rd_interval)
        
        plt.show()
    
    else:
        time.sleep(rd_interval)
        
       
    if ((ii % wrt_interval) == 0): 
        if writefile:
            np.savez(wrt_filename, dta = dta)
    
    
    
    
